# Brocade ML2 Mechanism driver from ML2 plugin

* N.B.: Please see Prerequisites section  regarding ncclient (netconf client library)
* Supports VCS (Virtual Cluster of Switches)
* Issues/Questions/Bugs: sharis@brocade.com, rmadapur@brocade.com

   1. VDX 67xx series of switches
   2. VDX 87xx series of switches

ML2 plugin requires mechanism driver to support configuring of hardware switches.
Brocade Mechanism for ML2 uses NETCONF at the backend to configure the Brocade switch.
Currently the mechanism drivers support VLANs only.
```python

             +------------+        +------------+          +-------------+
             |            |        |            |          |             |
   Neutron   |            |        |            |          |   Brocade   |
     v2.0    | Openstack  |        |  Brocade   |  NETCONF |  VCS Switch |
         ----+ Neutron    +--------+  Mechanism +----------+             |
             | ML2        |        |  Driver    |          |  VDX 67xx   |
             | Plugin     |        |            |          |  VDX 87xx   |
             |            |        |            |          |             |
             |            |        |            |          |             |
             +------------+        +------------+          +-------------+

```
## Topology 
Shows the topology, Compute and Controller nodes are connected to VDX devices.
On each compute resource(ie hosts running OVS), a bridge is created say br-em4 
containing the port(nic) that is connected to VDX.

```python

 +--------------------------+                      +--------------------------+  
 |                          |                      |                          |  
 |  Controller(10.37.21.127)|                      |   Compute (10.37.21.126) |  
 |                          |                      |                          |  
 +-------------+------------+                      +--------------+-----------+  
               | em4                                              | em4          
               |                                                  |              
               |                                                  |              
               |                                                  |              
               | 136/0/21                                         | 135/0/21     
+--------------+-------------+                      +-------------+-------------+
|                            |                      |                           |
| VDX(10.37.18.136)          |----------------------| VDX (10.37.18.135)        |
|                            |                      |                           |
+----------------------------+                      +---------------------------+


```
Note: All interfaces on VDX connected to Compute Resources should have the 
port-profile mode set using CLI command

```python
interface TenGigabitEthernet 135/0/21
 port-profile-port
 no shutdown
!
interface TenGigabitEthernet 136/0/21
 port-profile-port
 no shutdown
!
```

Configuration

In order to use this mechnism the brocade configuration file needs to be edited with the appropriate
configuration information:

        % cat /etc/neutron/plugins/ml2/ml2_conf_brocade.ini
        [switch]
        username = admin
        password = password
        address  = <switch mgmt ip address>
        ostype   = NOS
        osversion = autodetect
        physical_networks = physnet1
        rbridge_id = <rbridge_id on which SVI/L3 routing happens>

Additionally the brocade mechanism driver needs to be enabled from the ml2 config file:

       % cat /etc/neutron/plugins/ml2/ml2_conf.ini

       [ml2]
       tenant_network_types = vlan
       type_drivers = local,flat,vlan,gre,vxlan
       mechanism_drivers = openvswitch,brocade
       # OR mechanism_drivers = openvswitch,linuxbridge,hyperv,brocade
       ...
       ...
       ...
       [ml2_type_vlan]
       network_vlan_ranges = physnet1:100:500
       ...
       ...
       [ovs]
       bridge_mappings = physnet1:br-em4
       ...
       ...

vlan range is specified as 100:500 and made available through    


Required L2 Agent

This mechanism driver works in conjunction with an L2 Agent. The agent should be
loaded as well in order for it to configure the virtual network int the host machine.
Please see the configuration above. Atleast one of linuxbridge or openvswitch must be specified.

Hardware L3 Router (SVI)

Brocade Hardaware supports SVI (Switch Virtual Interface) which provides ASIC level
routing/gateway functionality in the switch for configured VLANs. 
This Service plugin provides support for this feature which enables line rate routing/gateway 
functionality.

l3_router_plugin.py provides a hardware based l3 router.

```python
cat /etc/neutron/neutron.conf
service_plugins = networking_brocade.vdx.services.l3_router.l3_router_plugin.BrocadeSVIPlugin
There is an entry for BrocadeSVIPlugin
```
Please refer to: https://blueprints.launchpad.net/neutron/+spec/brocade-l3-svi-service-plugin
for more details